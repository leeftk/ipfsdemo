const IPFS = require('ipfs')

const node = new IPFS()

node.on('ready', async () => {
  const version = await node.version()

  console.log('Version:', version.version)
})

node.on('ready', async () => {
  const version = await node.version()

  console.log('Version:', version.version)

  const filesAdded = await node.add({
    path: 'heo.txt',
    content: Buffer.from(<p> heloo world</p>)
  })

  console.log('Added file:', filesAdded[0].path, filesAdded[0].hash)
})