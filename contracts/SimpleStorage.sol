pragma solidity >=0.4.20;

contract SimpleStorage {
  string ipfsHash;

  function set(string memory x) public {
    ipfsHash = x;
  }

  function get() public view  returns (string memory) {
    return ipfsHash;
  }
}
